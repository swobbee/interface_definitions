# SPC-850WH-800100-FP und SPC-500WH-750066-FP von Shenzhen Super Power Technology CO LTD

Alle dazugehörigen Dokumente befinden sich im [Teams](https://teams.microsoft.com/_#/files/Super%20Power?threadId=19%3A47179ec151fb49e697637486803756b4%40thread.skype&ctx=channel&context=Super%2520Power&rootfolder=%252Fsites%252FElektronikentwicklung%252FFreigegebene%2520Dokumente%252FSuper%2520Power).


## CAN-Bus Setup
Die Anschlüsse für CAN-High und CAN-Low sind bei den unterschiedlichen Modellen unterschiedlich und stehen bei den neuen Ladegeräten (Oktober 2021) auf dem Gehäuse.
- SPC-850WH-800100-FP:
	- CAN-High: Rechts (rot)
	- CAN-Low:  Links  (schwarz)
- SPC-500WH-750066-FP:
	- CAN-High: Links  (schwarz)
	- CAN-Low:  Rechts (rot)

Für beide Modelle beträgt die Bitrate 250k.


## Verbindung prüfen

Das Ladegerät sendet von sich aus keine Nachrichten (obwohl es dies wohl eigentlich tun sollte).
Es sollte aber auf Anfragen antworten. Der Befehl für Node ID = 0 lautet:

```bash
$ cansend can0 15000100#0000000000000000
```

```bash
$ candump can0
  can0  15000100   [8]  00 00 00 00 00 00 00 00
  can0  15000100   [8]  00 0A 00 0A 00 00 01 4A
```

Im Gegensatz zu SPC-850WH-800100-FP akzeptiert SPC-500WH-750066-FP keine Remote Frames.

Dieser Befehl scheint keine Broadcast-Adresse zu haben.

Wenn das Ladegerät nicht antwortet, prüfe Verkabelung, Abschlusswiderstand, Bitrate und probiere eine andere Node-ID.


## Aufbau der CAN-IDs

Die IDs der CAN-Botschaften sind 8-stellige Hex-Zahlen, die nach folgendem Prinzip aufgebaut sind: `RWNNMMSS`

- `RW`: 0x15 zum Lesen von Werten, 0x16 zum Schreiben von Werten
- `NN`: Node-ID
- `MM`: Message
- `SS`: Submessage


## Ändern der Node ID

Die Default Node ID ist 0.
(Laut Dokumentation: "The charger address Id is 1 by default", aber charger address Id = Node ID + 1, also kein Widerspruch.)

Die SPC-500WH-750066-FP Ladegeräte haben ein Mäuseklavier, an dem man laut Spezifikation die Node ID einstellen können sollte.
V3 ignoriert das Mäuseklavier, stattdessen kann die Node ID über CAN eingestellt werden. Für diesen Befehl kann 0xFF als Broadcast verwendet werden.
Bei V4 wird die Node ID über das Mäuseklavier eingestellt, die Einstellung über CAN-Bus ist nicht mehr möglich.

```bash
$ cansend can0 16FF0400#420000000000000000
```

Wenn die Node ID geändert worden ist antwortet das Ladegerät.
(SPC-850WH-800100-FP antwortet auch, wenn das Ladegerät bereits die gewünschte Node ID hatte.)
Die Antwort hat die gleiche CAN-ID wie der Befehl zum Ändern der Node-ID.

Für SPC-850WH-800100-FP: Das erste Daten-Byte ist die neue Node ID (wie in dem pdf-Dokument gesagt).
Die Botschaft ist trotzdem nicht gleiche Botschaft, Byte 8 ist 0xF0.
Es wird nur auf Botschaften an die Broadcast-Node-ID reagiert.

Für SPC-500WH-750066-FP: Das erste Daten-Byte ist immer 01 (wie Jessie in ihrer E-Mail vom 30.10.2021 gesagt hat).
Daher ist ist die neue Node ID in der Antwort nicht ersichtlich.
Es wird auf Botschaften an die Broadcast-Node-ID und die aktuelle Node ID reagiert.

```bash
  can0  16FF0400   [8]  42 00 00 00 00 00 00 00
  can0  16FF0400   [8]  01 00 00 00 00 00 00 00
```

Die Node ID bleibt erhalten, wenn das Ladegerät von der Spannungsversorgung getrennt wird.


## Laden

Entgegen dem ursprünglichen Eindruck ist es wohl doch nicht notwendig, dass ein Akku angeschlossen ist, um die Ausgangsspannung freizuschalten.

Remote-Mode aktivieren (was auch immer das heißen mag, scheint nicht erforderlich zu sein für die neuen Geräte [Oktober 2021]):
```bash
$ cansend can0 '16000308#0000aaaa00000000'
```

Strom und Spannung einstellen und aktivieren (hier: 58,8V und 5A):
```bash
$ cansend can0 '16000306#024c003200000101'
```

Laden starten:
```bash
$ cansend can0 '16000307#1234000000000000'
```

Damit die Ausgangsspannung aktiv bleibt, muss ein Keep-Alive-Signal gesendet werden.
Offiziell muss das Kommando um das Laden zu starten alle 5s (und nicht in kürzeren Abständen!) gesendet werden (laut einer E-Mail von Jessie vom 29.05.2021):
```bash
$ cansend can0 '16000307#1234000000000000'
```

Erfahrungsgemäß reicht es aber jede Sekunde die Daten abzufragen (EDIT 26.10.2021: mit den neuen Geräten reicht das nicht mehr, diese neuen Geräte haben nicht mehr den Widerstand, der abgeraucht ist, haben aber die gleiche Model Nummer: SPC-850WH-800100-FP):
```bash
$ cansend can0 '15000100#remote'
```

Ohne Keep-Alive-Signal wird die Aussgangsspannung automatisch nach ca. eineinhalb Minuten deaktiviert.
