#!/usr/bin/env python3

'''
Baudrate: 115200
Data: 8
Stop: seems to work with both 1 and 2
Parity: None
'''

import typing

HEADER = [0x5A, 0xA5]
SOURCE = 0x3D
TARGET = 0x22
COMMAND_WORD = 0x01

TYPE_DATA_IN = typing.Union[str, typing.List[int]]


# ---------- generate commands ----------

def generate_command(address: int, number_bytes_of_answer_data: int, name: str) -> None:
	data = HEADER + [1, SOURCE, TARGET, COMMAND_WORD, address, number_bytes_of_answer_data]
	print_command(name, data)

def print_command(name: str, data: TYPE_DATA_IN) -> None:
	data = parse_data(data)
	chk = checksum(data)
	data.append(chk & 0xFF)
	data.append((chk >> 8) & 0xFF)
	if not data[:len(HEADER)] == HEADER:
		data = HEADER + data
	print("%s: %s" % (name, " ".join("%02x" % b for b in data)))

def checksum(data: TYPE_DATA_IN) -> int:
	data = parse_data(data)
	if data[:len(HEADER)] == HEADER:
		data = data[len(HEADER):]
	out = sum(data)
	out = invert(out)
	return out

def parse_data(data: TYPE_DATA_IN) -> typing.List[int]:
	if isinstance(data, list):
		return data
	return [int(b, base=16) for b in data.split()]

def invert(number: int, n: int=16) -> int:
	data = "{:0{n}b}".format(number, n=n)
	data = data.replace('0', 'O').replace('1', '0').replace('O', '1')
	out = int(data, base=2)
	return out


# ---------- parse answers ----------

class Reply:

	def __init__(self, msg: str) -> None:
		msg_l = [int(byte, base=16) for byte in msg.split()]
		assert msg_l[0] == HEADER[0]
		assert msg_l[1] == HEADER[1]
		data_len = msg_l[2]
		source   = msg_l[3]
		target   = msg_l[4]
		command  = msg_l[5]
		address  = msg_l[6]
		data     = msg_l[7:-2]
		chk      = msg_l[-2] + (msg_l[-1] << 8)
		assert data_len == len(data)
		# we have received this message, not sent it so source and target are swapped
		assert source == TARGET
		assert target == SOURCE
		assert chk == checksum(msg_l[:-2])

		self.address = address
		self.data = data

	def get_unsigned_int(self) -> int:
		return int.from_bytes(self.data, byteorder='little')

	def get_signed_int(self) -> int:
		val = self.get_unsigned_int()
		bits = 8 * len(self.data)
		if val & (1 << (bits - 1)):
			return invert(val, n=bits) + 1
		return val

	def get_str(self) -> str:
		return ''.join(chr(byte) if byte >= ord(' ') and byte <= ord('~') else '?' for byte in self.data)

	def __str__(self) -> str:
		data = ' '.join('%02x' % b for b in self.data)
		return f'{type(self).__name__}{{address: {self.address}, data: {data}}}'


# ---------- main ----------

if __name__ == '__main__':
	print(__doc__)

	generate_command(0x10, 14, "Serial_Number")
	generate_command(0x17,  2, "Version")
	generate_command(0x18,  2, "Capacity")
	generate_command(0x19,  2, "Total Capacity")
	generate_command(0x1A,  2, "Design Voltage")
	generate_command(0x1B,  2, "Cycle Times")
	generate_command(0x1C,  2, "Charge Times")
	generate_command(0x1D,  2, "Charge Capacity Low")
	generate_command(0x1E,  2, "Charge Capacity High")
	generate_command(0x1F,  2, "Over Discharge Times and Over Charge Times")
	generate_command(0x30,  2, "State Mask")
	generate_command(0x33,  2, "Current in 10 mA")
	generate_command(0x34,  2, "Voltage in 10 mV")
	generate_command(0x35,  2, "2 Temperatures")
	generate_command(0x36,  2, "Balance Status")
	generate_command(0x37,  2, "Undervoltage condition of battery cell")
	generate_command(0x38,  2, "Overvoltage condition of battery cell")
	generate_command(0x39,  2, "Coulombmeter capacity")
	generate_command(0x3A,  2, "Voltmeter capacity")
	generate_command(0x3B,  2, "SOH")
	generate_command(0x3E,  2, "SOC")
	for i in range(16):
		generate_command(0x40+i,  2, "cell voltage %s" % (i+1))
	generate_command(0x51,  2, "Version")
	print()
	generate_command(0x33, 6, "Current Voltage Temperatures")
	generate_command(0x1D, 4, "Charge Capacity")
	generate_command(0x10, 2, "SN1")
	generate_command(0x11, 2, "SN2")
	generate_command(0x12, 2, "SN3")
	generate_command(0x13, 2, "SN4")
	generate_command(0x14, 2, "SN5")
	generate_command(0x15, 2, "SN6")
	generate_command(0x16, 2, "SN7")

	print('')
	print('serial number:  ', Reply('5A A5 0E 22 3D 04 10 42 50 44 43 56 32 31 42 52 42 30 35 38 37 02 FC').get_str())
	print('SOC in %:       ', Reply('5A A5 02 22 3D 04 3E 5D 00 FF FE').get_signed_int())
	print('current in 10mA:', Reply('5A A5 02 22 3D 04 33 00 00 67 FF').get_signed_int())
	print('voltage in 10mV:', Reply('5A A5 02 22 3D 04 34 E6 0F 71 FE').get_signed_int())
